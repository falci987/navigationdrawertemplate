package it.falcy.testnavdrawer;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class EmptyActivity extends AppCompatActivity {

    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private String TAG = "### EmptyActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty);

        //attivaPubblicitaTest();
        attivaPubblicitaProd();
        listenerAd();
    }


    private void attivaPubblicitaTest() {

        mAdView = (AdView) findViewById(R.id.adView6);
        //ADs per TEST
        String TEST_APP_ID = "ca-app-pub-3940256099942544/6300978111"; //id di test
        MobileAds.initialize(this, TEST_APP_ID);

        //ca-app-pub-3940256099942544/6300978111 //Attiva il banner di prova dentro adView. Da scrivere nell'XML
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712"); //id di test
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    private void attivaPubblicitaProd() {
        mAdView = (AdView) findViewById(R.id.adView6);
        String PROD_ID = getResources().getString(R.string.ADMOB_APP_ID_PROD);
        MobileAds.initialize(this, PROD_ID);

        //TODO: SCRIVI ca-app-pub-8689451173566961/9190543591 nel componente adView per attivarlo in prod// BannerEmptyActivity
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-8689451173566961/9816820435"); //InterstitialNavDraw
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    private void listenerAd(){
        final String ad1="InterstitialAd ";
        final String ad2="BannerAd ";

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Context context = getApplicationContext();
                CharSequence text = "Welcome";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                Log.i(TAG,ad1+"onAdLoaded");
                int c = ((MyCounter) getApplicationContext()).getContaADcaricati();
                c = c+1;
                ((MyCounter) getApplicationContext()).setContaADcaricati(c);
                Log.i(TAG,"Conta: "+c);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.i(TAG,ad1+"onAdFailedToLoad");
            }

            @Override
            public void onAdOpened() {
                Log.i(TAG,ad1+"onAdOpened");
            }

            @Override
            public void onAdLeftApplication() {
                Log.i(TAG,ad1+"onAdLeftApplication");
            }

            @Override
            public void onAdClosed() {
                Log.i(TAG,ad1+"onAdClosed");
            }
        });

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.i(TAG,ad2+"onAdLoaded");
                int c = ((MyCounter) getApplicationContext()).getContaADcaricati();
                c = c+1;
                ((MyCounter) getApplicationContext()).setContaADcaricati(c);
                Log.i(TAG,"Conta: "+c);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.i(TAG,ad2+"onAdFailedToLoad");
            }

            @Override
            public void onAdOpened() {
                Log.i(TAG,ad2+"onAdOpened");
            }

            @Override
            public void onAdLeftApplication() {
                Log.i(TAG,ad2+"onAdLeftApplication");
            }

            @Override
            public void onAdClosed() {
                Log.i(TAG,ad2+"onAdClosed");
            }
        });
    }

}
