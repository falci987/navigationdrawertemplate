package it.falcy.testnavdrawer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private String TAG = "### MainActivity";
    private TextView myText4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        //Faccio lampeggiare il pallino
        ImageView image = (ImageView) findViewById(R.id.immagine_pallino);
        Animation animation = new AlphaAnimation((float) 0.5, 0); // Change alpha from fully visible to invisible
        animation.setDuration(1000); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter
        // animation
        // rate
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation
        // infinitely
        animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the
        // end so the button will
        // fade back in
        image.startAnimation(animation);


        TextView myText = (TextView) findViewById(R.id.info_text_ztl_centrale);
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/product_sans_regular.ttf");
        TextView myText2 = (TextView) findViewById(R.id.text_card_sem_rossi);
        TextView myText3 = (TextView) findViewById(R.id.text_card_sem_verdi);
         myText4 = (TextView) findViewById(R.id.text_card_sem_rosse);

        myText.setTypeface(type);
        myText2.setTypeface(type);
        myText3.setTypeface(type);
        myText4.setTypeface(type);

        //attivaPubblicitaTest();
        attivaPubblicitaProd();
        listenerAd();

        int c = ((MyCounter) getApplicationContext()).getContaADcaricati();
        myText4.setText("Loaded: "+c);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent toSettings = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(toSettings);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();



        if (id == R.id.nav_camera) {
            showInterstitialAd();
            Intent toBottNav = new Intent(getApplicationContext(), BottNavActivity.class);
            startActivity(toBottNav);
        } else if (id == R.id.nav_gallery) {
            Intent toMaps = new Intent(getApplicationContext(), MapsActivity.class);
            startActivity(toMaps);

        } else if (id == R.id.nav_slideshow) {
            showInterstitialAd();
            Intent toEset = new Intent(getApplicationContext(), EasySettings.class);
            startActivity(toEset);

        } else if (id == R.id.nav_manage) {
            Intent toEmp = new Intent(getApplicationContext(), EmptyActivity.class);
            startActivity(toEmp);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void showInterstitialAd(){
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d(TAG, "The interstitial wasn't loaded yet.");
        }
    }

    private void attivaPubblicitaTest() {

        mAdView = (AdView) findViewById(R.id.adView1);
        //ADs per TEST
        String TEST_APP_ID = "ca-app-pub-3940256099942544/6300978111"; //id di test
        MobileAds.initialize(this, TEST_APP_ID);

        //ca-app-pub-3940256099942544/6300978111 //Attiva il banner di prova dentro adView. Da scrivere nell'XML
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712"); //id di test
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    private void attivaPubblicitaProd() {
        mAdView = (AdView) findViewById(R.id.adView1);
        String PROD_ID = getResources().getString(R.string.ADMOB_APP_ID_PROD);
        MobileAds.initialize(this, PROD_ID);

        //TODO: SCRIVI ca-app-pub-8689451173566961/6175182228 nel componente adView per attivarlo in prod //BannerMainActivity
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-8689451173566961/9816820435"); //InterstitialNavDraw
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    private void listenerAd(){
        final String ad1="InterstitialAd ";
        final String ad2="BannerAd ";

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Context context = getApplicationContext();
                CharSequence text = "Welcome";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                Log.i(TAG,ad1+"onAdLoaded");
                int c = ((MyCounter) getApplicationContext()).getContaADcaricati();
                c = c+1;
                ((MyCounter) getApplicationContext()).setContaADcaricati(c);
                Log.i(TAG,"Conta: "+c);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.i(TAG,ad1+"onAdFailedToLoad");
            }

            @Override
            public void onAdOpened() {
                Log.i(TAG,ad1+"onAdOpened");
            }

            @Override
            public void onAdLeftApplication() {
                Log.i(TAG,ad1+"onAdLeftApplication");
            }

            @Override
            public void onAdClosed() {
                Log.i(TAG,ad1+"onAdClosed");
            }
        });

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.i(TAG,ad2+"onAdLoaded");
                int c = ((MyCounter) getApplicationContext()).getContaADcaricati();
                c = c+1;
                ((MyCounter) getApplicationContext()).setContaADcaricati(c);
                Log.i(TAG,"Conta: "+c);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.i(TAG,ad2+"onAdFailedToLoad");
            }

            @Override
            public void onAdOpened() {
                Log.i(TAG,ad2+"onAdOpened");
            }

            @Override
            public void onAdLeftApplication() {
                Log.i(TAG,ad2+"onAdLeftApplication");
            }

            @Override
            public void onAdClosed() {
                Log.i(TAG,ad2+"onAdClosed");
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        int c = ((MyCounter) getApplicationContext()).getContaADcaricati();
        myText4.setText("Loaded: "+c);
    }
}
