package it.falcy.testnavdrawer;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap ztlMap;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private String TAG = "### MapsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //attivaPubblicitaTest();
        attivaPubblicitaProd();
        listenerAd();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        ztlMap = googleMap;


        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(45.069, 7.68241))
                .zoom(14)
                .bearing(0)
                .tilt(0)
                .build();

        ztlMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void showInterstitialAd(){
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d(TAG, "The interstitial wasn't loaded yet.");
        }
    }

    private void attivaPubblicitaTest() {

        mAdView = (AdView) findViewById(R.id.adView3);
        //ADs per TEST
        String TEST_APP_ID = "ca-app-pub-3940256099942544/6300978111"; //id di test
        MobileAds.initialize(this, TEST_APP_ID);

        //ca-app-pub-3940256099942544/6300978111 //Attiva il banner di prova dentro adView. Da scrivere nell'XML
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712"); //id di test
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    private void attivaPubblicitaProd() {
        mAdView = (AdView) findViewById(R.id.adView3);
        String PROD_ID = getResources().getString(R.string.ADMOB_APP_ID_PROD);
        MobileAds.initialize(this, PROD_ID);

        //TODO: SCRIVI ca-app-pub-8689451173566961/7892939645 nel componente adView per attivarlo in prod //BannerMapsActivity
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-8689451173566961/9816820435"); //InterstitialNavDraw
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    private void listenerAd(){
        final String ad1="InterstitialAd ";
        final String ad2="BannerAd ";

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Context context = getApplicationContext();
                CharSequence text = "Welcome";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                Log.i(TAG,ad1+"onAdLoaded");
                int c = ((MyCounter) getApplicationContext()).getContaADcaricati();
                c = c+1;
                ((MyCounter) getApplicationContext()).setContaADcaricati(c);
                Log.i(TAG,"Conta: "+c);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.i(TAG,ad1+"onAdFailedToLoad");
            }

            @Override
            public void onAdOpened() {
                Log.i(TAG,ad1+"onAdOpened");
            }

            @Override
            public void onAdLeftApplication() {
                Log.i(TAG,ad1+"onAdLeftApplication");
            }

            @Override
            public void onAdClosed() {
                Log.i(TAG,ad1+"onAdClosed");
            }
        });

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.i(TAG,ad2+"onAdLoaded");
                int c = ((MyCounter) getApplicationContext()).getContaADcaricati();
                c = c+1;
                ((MyCounter) getApplicationContext()).setContaADcaricati(c);
                Log.i(TAG,"Conta: "+c);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.i(TAG,ad2+"onAdFailedToLoad");
            }

            @Override
            public void onAdOpened() {
                Log.i(TAG,ad2+"onAdOpened");
            }

            @Override
            public void onAdLeftApplication() {
                Log.i(TAG,ad2+"onAdLeftApplication");
            }

            @Override
            public void onAdClosed() {
                Log.i(TAG,ad2+"onAdClosed");
            }
        });
    }


}
